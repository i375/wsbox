/*
The MIT License

Copyright � 2016 Ivane Gegia http://ivane.info

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:


The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.


THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include "WSBServerSocket.h"

static struct WSBServerSocket* create(WSBOnNewClientHandler onNewClientHandler)
{
    struct WSBServerSocket *self = calloc(1, sizeof(struct WSBServerSocket));
    self->onNewClientHandler = onNewClientHandler;

    if (self == NULL)
    {
        return NULL;
    }

    self->serverSocket = _WSBSocket.create();

    return self;
}

static DWORD WINAPI newClientThread(_In_ struct WSBServerSocket* self)
{
    self->onNewClientHandler(self->newClientSock);

    return 0;
}

static DWORD WINAPI acceptClientConnectionsThread(_Inout_ struct WSBServerSocket* self)
{
    do
    {

        SOCKET clientSocket = accept(self->serverSocket->socketHandle, NULL, NULL);

        if(clientSocket == INVALID_SOCKET)
        {
            printf("Error accepting client socket: %ld", WSAGetLastError());
            continue;
        }
        
        self->newClientSock = _WSBSocket.create();
        self->newClientSock->socketHandle = clientSocket;
        
        // Passing new client socket to helper thread, so that 
        // newClientSock consumer doesn't block client acceptor thread,
        // current thread.
        CreateThread(NULL, 0, newClientThread, self, 0, 0);


    } while (1);
}

static int startListeningOnPort(_Inout_ struct WSBServerSocket *self, char *port)
{
    self->serverSocket->result = WSAStartup(MAKEWORD(2, 2), &self->serverSocket->wsaData);

    if (self->serverSocket->result != 0)
    {
        printf("WSAStartup failed with error: %ld", WSAGetLastError());

        return -1;
    }

    ZeroMemory(&self->serverSocket->addrHits, sizeof(struct addrinfo));
    self->serverSocket->addrHits.ai_family = AF_INET;
    self->serverSocket->addrHits.ai_protocol = IPPROTO_TCP;
    self->serverSocket->addrHits.ai_flags = AI_PASSIVE;
    self->serverSocket->addrHits.ai_socktype = SOCK_STREAM;

    self->serverSocket->result = getaddrinfo(NULL, port, &self->serverSocket->addrHits, &self->serverSocket->addInfoResults);

    if(self->serverSocket->result != 0)
    {
        printf("Couldn't resovle addr information");
        WSACleanup();
        return -1;
    }

    self->serverSocket->socketHandle = socket(
        self->serverSocket->addInfoResults[0].ai_family,
        self->serverSocket->addInfoResults[0].ai_socktype,
        self->serverSocket->addInfoResults[0].ai_protocol);
    
    if(self->serverSocket->socketHandle == INVALID_SOCKET)
    {
        printf("Failed creating server socket with error: %ld", WSAGetLastError());
        freeaddrinfo(self->serverSocket->addInfoResults);
        closesocket(self->serverSocket->socketHandle);
        WSACleanup();

        return -1;
    }

    self->serverSocket->result = bind(
        self->serverSocket->socketHandle, 
        self->serverSocket->addInfoResults[0].ai_addr, 
        self->serverSocket->addInfoResults[0].ai_addrlen);
    
    if(self->serverSocket->result == SOCKET_ERROR)
    {
        printf("Binding socket with address failed, with error: %ld", WSAGetLastError());
        freeaddrinfo(self->serverSocket->addInfoResults);
        closesocket(self->serverSocket->socketHandle);
        WSACleanup();

        return -1;
    }

    freeaddrinfo(self->serverSocket->addInfoResults);

    self->serverSocket->result = listen(self->serverSocket->socketHandle, SOMAXCONN);

    if (self->serverSocket->result == SOCKET_ERROR)
    {
        printf("Couldn't start listening on socket, with error: %ld", WSAGetLastError());
        closesocket(self->serverSocket->socketHandle);
        WSACleanup();

        return -1;
    }

    CreateThread(NULL, 0, acceptClientConnectionsThread, self, 0, NULL);
    
    return 0;
}

static void _free(struct WSBServerSocket *self)
{
    self->serverSocket->result = closesocket(self->serverSocket->socketHandle);

    if(self->serverSocket->result == SOCKET_ERROR)
    {
        printf("Error while closing socket: %ld", self->serverSocket->result);
    }

    WSACleanup();

    _WSBSocket.free(self->serverSocket);
}

struct __WSBServerSocket _WSBServerSocket = {
    .create = create,
    .free = _free,
    .startListeningOnPort = startListeningOnPort
};
/*
The MIT License

Copyright � 2016 Ivane Gegia http://ivane.info

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:


The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.


THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include "WSB.h"

// Server socket
void onNewClient(struct WSBSocket *clientSocket)
{
    printf("new client connected: %ld \n", clientSocket->socketHandle);

    send(clientSocket->socketHandle, "hello\n\r", strlen("hello\n\r")+1, MSG_OOB);
}

struct WSBServerSocket* startServer()
{
    struct WSBServerSocket *srvSocket = _WSBServerSocket.create(onNewClient);

    _WSBServerSocket.startListeningOnPort(srvSocket, "8080");

    return srvSocket;
}

// Client socket

void clientSocketOnData(_In_ struct WSBClientSocket *sender, _In_ char *data, _In_ int dataSize)
{
    printf("Data from server %s", data);
}

struct WSBClientSocket* startClient()
{
    struct WSBClientSocket *clientSocket = _WSBClientSocket.create();

    _WSBClientSocket.connectToServer(clientSocket, "localhost", "8080", clientSocketOnData);


    return clientSocket;
}



int main(int argc, char* argv)
{
    struct WSBServerSocket *srvSocket = startServer();
    struct WSBClientSocket *clntSocket = startClient();

    getchar();

    _WSBServerSocket.free(srvSocket);
    _WSBClientSocket.free(clntSocket);

    return 0;
}
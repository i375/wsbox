﻿/*
The MIT License

Copyright © 2016 Ivane Gegia http://ivane.info

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:


The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.


THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include "WSBClientSocket.h"

struct WSBClientSocket* create()
{
    struct WSBClientSocket *self = calloc(1, sizeof(struct WSBClientSocket));

    if(self == NULL)
    {
        return NULL;
    }

    self->clientSocket = _WSBSocket.create();

    return self;
}

DWORD WINAPI recvThread(_In_ struct WSBClientSocket *self)
{
    do
    {

        self->sentRecvedDataSize = recv(
            self->clientSocket->socketHandle, 
            self->sendRecvBuffer, 
            WSBClientSocket_sendRecvBuffer_size, 
            0);

        if (self->sentRecvedDataSize > 0)
        {
            self->onData(self, self->sendRecvBuffer, self->sentRecvedDataSize);
            //printf("bytes received on client socket: %d\n", sot->iResult);
        }
        else if (self->sentRecvedDataSize == 0)
        {
            printf("Server closed connection.\n");
        }
        else
        {
            printf("Socket recv failed with error: %d\n", WSAGetLastError());
            WSACleanup();
        }
        
    } while (self->sentRecvedDataSize > 0);

    return 0;
}

static int connectToServer(
    _In_ struct WSBClientSocket *self,
    _In_ char* serverAddress,
    _In_ char* serverPort,
    _In_ WSBClientSocketOnData onData)
{
    self->onData = onData;

    self->clientSocket->result = WSAStartup(MAKEWORD(2, 2), &self->clientSocket->wsaData);

    if(self->clientSocket->result != 0)
    {
        printf("Error on WSAStartup: %ld", WSAGetLastError());
        WSACleanup();
        return -1;
    }

    ZeroMemory(&self->clientSocket->addrHits, sizeof(self->clientSocket->addrHits));

    self->clientSocket->addrHits.ai_family = AF_UNSPEC;
    self->clientSocket->addrHits.ai_socktype = SOCK_STREAM;
    self->clientSocket->addrHits.ai_protocol = IPPROTO_TCP;

    self->clientSocket->result = getaddrinfo(
        serverAddress, 
        serverPort, 
        &self->clientSocket->addrHits, 
        &self->clientSocket->addInfoResults);

    if(self->clientSocket->result != 0)
    {
        printf("Error getting server address: %ld", WSAGetLastError());

        freeaddrinfo(self->clientSocket->addInfoResults);
        WSACleanup();
        return -1;
    }

    for (struct addrinfo *addr = self->clientSocket->addInfoResults; 
        addr != NULL;
        addr = addr->ai_next)
    {
        self->clientSocket->socketHandle = socket(addr->ai_family, addr->ai_socktype, addr->ai_protocol);

        if(self->clientSocket->socketHandle == INVALID_SOCKET)
        {
            printf("Error creating socket: %ld", WSAGetLastError());
            freeaddrinfo(self->clientSocket->addInfoResults);
            WSACleanup();
            return -1;
        }

        self->clientSocket->result = connect(self->clientSocket->socketHandle, addr->ai_addr, addr->ai_addrlen);

        if(self->clientSocket->result == SOCKET_ERROR)
        {
            closesocket(self->clientSocket->socketHandle);
            self->clientSocket->socketHandle = INVALID_SOCKET;
            continue;
        }

    }

    freeaddrinfo(self->clientSocket->addInfoResults);

    if(self->clientSocket->socketHandle == INVALID_SOCKET)
    {
        printf("Cound't connect to server: %ld", WSAGetLastError());
        WSACleanup();

        return -1;
    }

    //Socket is valid start recv thread
    CreateThread(NULL, 0, recvThread, self, 0, NULL);

    self->running = true;
    
    return 0;
}

static void _send(_In_ struct WSBClientSocket *self, _In_ char* data, _In_ int dataLength)
{
    send(self->clientSocket->socketHandle, data, dataLength, MSG_OOB);
}

static void _free(struct WSBClientSocket *self)
{
    _WSBSocket.free(self->clientSocket);
    free(self);
}

struct __WSBClientSocket _WSBClientSocket = {
    .create = create,
    .connectToServer = connectToServer,
    .send = _send,
    .free = _free
};
#WS Box#

Windows Sockets 2 TCP/IP, a bit convenient wrapper. 

Verions 0.1

*Copyright (c) 2016 Ivane Gegia http://ivane.info*

**Programming language:** C

**Library prefix:** WSB

**License:** MIT (see [LICENSE.txt](LICENSE.txt))

**Main file:** WSB.h

#Usage example:#

```c
#include "WSB.h"

// Server socket
void onNewClient(struct WSBSocket *clientSocket)
{
    printf("new client connected: %ld \n", clientSocket->socketHandle);

    send(clientSocket->socketHandle, "hello\n\r", strlen("hello\n\r")+1, MSG_OOB);
}

struct WSBServerSocket* startServer()
{
    struct WSBServerSocket *srvSocket = _WSBServerSocket.create(onNewClient);

    _WSBServerSocket.startListeningOnPort(srvSocket, "8080");

    return srvSocket;
}

// Client socket

void clientSocketOnData(_In_ struct WSBClientSocket *sender, _In_ char *data, _In_ int dataSize)
{
    printf("Data from server %s", data);
}

struct WSBClientSocket* startClient()
{
    struct WSBClientSocket *clientSocket = _WSBClientSocket.create();

    _WSBClientSocket.connectToServer(clientSocket, "localhost", "8080", clientSocketOnData);

    return clientSocket;
}

int main(int argc, char* argv)
{
    struct WSBServerSocket *srvSocket = startServer();
    struct WSBClientSocket *clntSocket = startClient();

    getchar();

    _WSBServerSocket.free(srvSocket);
    _WSBClientSocket.free(clntSocket);

    return 0;
}
```
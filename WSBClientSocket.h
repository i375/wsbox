﻿/*
The MIT License

Copyright © 2016 Ivane Gegia http://ivane.info

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:


The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.


THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#pragma once

#include "WSBSocket.h"

typedef void(*WSBClientSocketOnData)(_In_ struct WSBClientSocket *sender, _In_ char *data, _In_ int dataSize);

#define WSBClientSocket_sendRecvBuffer_size 512

struct WSBClientSocket
{
    struct WSBSocket *clientSocket;
    WSBClientSocketOnData onData;
    int running;
    char sendRecvBuffer[WSBClientSocket_sendRecvBuffer_size];
    int sentRecvedDataSize;
};

struct __WSBClientSocket
{
    struct WSBClientSocket*(*create)();

    int(*connectToServer)(
        _In_ struct WSBClientSocket *self, 
        _In_ char* serverAddress, 
        _In_ char* serverPort, 
        _In_ WSBClientSocketOnData onData);

    void(*send)(_In_ struct WSBClientSocket *self, _In_ char* data, _In_ int dataLength);

    void(*free)(_In_ struct WSBClientSocket *self);
};

extern struct __WSBClientSocket _WSBClientSocket;